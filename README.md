# Nali - N100 Traxmate Demo Application
This CP-Flex application Provides basic location tracking functions optimized for Traxmate testing and demonstration 
activities.   It is based upon the *ped_tracker* application and supports the same command interface.   

**Version: v1.0<br/>**
**Release Date: 2021/07/29**

## Key Features  

1. **Manual locate mode (default)** - Single click locates.
2. **Continuous tracking mode** - Automatically sends locates every 5 minutes (300 seconds), 
  requiring uplink confirmation (NOTE: remove this for more optimal LoRaWAN management). Tracking is enabled whenever the 
  application is restarted.
3. Emergency Alert - User can send an emergency alert notice.
4. Location accuracy is set to coarse by default. 
5. Automatic battery status reports every 10% battery change (5% when battery critical).
6. Battery status indicator 
7. Network status indicator
8. Temperature report
9. Location measurement configuration control
10. Out of Network Caching (NEW)
11. Improved Network Link Checks

## Settings ##

### Cloud configurable non-volatile settings
The application supports non-volatile downlink settings for standard reporting interval.  Default is five (5) minutes. These values survives reboot or dead battery conditions.

### Network Link Checks
Network link Checks are performed using a combination of time and activity metrics.  Uplink checks are performed
in accordance to the following schedule.   If confirmed messages are sent, the schedule is updated.

|  Link Check Metric  |      Value            |
|:-------------------:|:---------------------:|
|    Time Interval    |   every 10 minutes    |
|    Uplink Activity  |   every 3 messages    |

These settings can be adjusted by implementing a custom ConfigLinkCheck.i

### Out of Network (OON) Caching 
The firmware now supports OON caching of messages, if the link check fails messages marked with the TPF_ARCHIVE
flag, will be stored until the metric becomes available.   Once the network is available at DR3 or higher, the
device will push three messages per minute until the archive is empty.  This is in addition to any 
messages stored in the uplink queue, which may contain up to 5 messages.


# User Interface 

## Normal Mode
Normal mode is entered when the device boots.

*Note:  If the tag is on the charger when booting, the entry to normal mode will be delayed 2 minutes*

**Normal Mode Button Descriptions**

The following table summarizes the user interface for the application.  See the sections
below for more detailed information regarding each button action.
 
| Button 1 | Button 2 | Description |
|---|---|---|
| very long press (> 5 seconds) |  very long press (> 5 seconds) |  Sends emergency (enabled) notification message.|
| 1 quick press | | Sends a location report  |
| 2 quick presses | | Sends status report (see Status Report below) |
| 3 quick presses | | Activate/Deactivate continuous tracking.  Activated by default whenever restarted. |
|  | 1 quick press   | Indicates battery level (see Battery Indicator below) |
|  | 2 quick presses | Indicates network coverage status (see Coverage Indicator below) |
|  | 5 quick presses | Enters location configuration mode (see Location Configuration Mode below).
|  | >15 sec. press | Resets the device. |

**General Operating Mode LED Descriptions** 

The following table indicates the meaning of the LEDs when
not previously triggered by pressing a button.

| LED #1 | LED #2 | Description |
|---|---|---|
| Green blink three times | Blue blink three times | Device restarted |
| Red blink three times quickly  | | Input invalid. |
| Green blink twice slowly | | Acquire Location.  
| Green blink three times slowly   | | Tracking activated.  |
| Red blink four times slowly      | | Tracking deactivated. ||
| | Green blink once | Valid input confirmed   |
| | orange slow blink every 10 sec. | Battery is charging. |
| | Green slow blink every 10 sec.  | Battery is charged. |
| | Green four(4) blinks every two minutes | Battery is nearly dead, place tag on charger |
| Orange blink five(5) times | Orange blink five(5) times | Enter location configuration mode (see below)|

### Status Report 

When activated the following status reports are sent:
 1. Tracking Status
 2. Battery Status (Discharging, Charging, Critical)
 3. Battery Level (% charged)
 3. Temperature  in degrees celsius.
 
### Battery Indicator 

Quickly pressing Button #2 will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2  | % Charge    | Description|
|---------|-------------|------------|
|  1 red  |   < 5%      | Battery level is critical,  charge immediately. |
|  1 green |  5%  - 30%  | Battery low  less than 30% remaining. |
|  2 green |  30% - 50%  | Battery is about 40% charged. |
|  3 green |  50% - 70%  | Battery is about 60% charged. |
|  4 green |  70% - 90%  | Battery  is about 80% charged. |
|  5 green |  > 90%      | Battery is fully charged. |

### Network Coverage Indicator 

Quickly pressing Button #2 twice will indicate the status of LoRaWAN network coverage.  

LED #2 will blink as follows to indicate coverage information

| LED #2  | Signal Strength (dBm) | Description                          |
|---------|-----------------------|--------------------------------------|
|  4 green |  -64  to -30    | Very strong signal strength                |
|  3 green |  -89  to  -65   | Good signal strength                       |
|  2 green |  -109  to -90   | Low signal strength                        |
|  1 green |  -120 to -110   | Very low signal strength                   |
|  1 red   |                 | Network unavailable (out of range)         |
|  2 red   |                 | LoRaWAN telemetry disabled. |


## Location Configuration Mode  
Enables specification of the location measurement reporting mode.  Device
can be configured to test various performance and measurement modes.  To enter location 
configuration mode, press button #2 quickly five times.  When mode is activated, the LEDs should 
blink purple 5 times.

**Location Configuration Mode Button Descriptions**
 
| Button 1 | Button 2 | Description |
|---|---|---|
| 1 long press |  |  Exit location configuration mode (see LEDs for feedback).
| 1 quick press|  |  Default performance mode (coarse for best battery life) |
| 2 quick press|  |  Coarse performance mode |
| 3 quick press|  |  Medium performance mode |
| 4 quick press|  |  Best performance mode   |
|   | 1 long press | Indicate location mode (performance first, then tech)
|   | 1 quick press | Default measurement mode (WiFi and BLE) |
|   | 2 quick press | WiFi only measurement mode |
|   | 3 quick press | BLE only measurement mode|
|   | 4 quick press | WIFI and BLE measurement mode |

**Location Configuration Mode LED Descriptions** 

| LED #1 | LED #2 | Description |
|---|---|---|
| Blue blink five(5) times | Blue blink five(5) times | Exit location configuration mode|
| N green blinks| | Indicates performance mode: 1-default, ... 4-best |
| | N green blinks| Indicates measurement mode: 1-default, ... 4-WiFi/BLE |
| Red blink three times | | Input invalid. |

---
*Copyright 2019-2021, Codepoint Technologies,* 
*All Rights Reserved*
  
