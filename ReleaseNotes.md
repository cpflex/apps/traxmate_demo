﻿# Traxmate Demo Application Release Notes

### V1.0.1.4  210729 ###
1) Updated App to compile with v1.2.3.0 of CP-Flex Platform 
2) Built with N100 V1.0.0.29 and N110 V1.0.0.5a firmware bundles. 
   See N100/N110 firmware documentation for additional information.
3) Fixes N110 initialization problems, which were causing bad persistence in PAWN apps.
4) Fixes N110 Communication Lockup after confirmation NACK.
7) Attempted fix to re-enable message archiving.

### V1.0.0.8a  210526 ###
1) Built with V1.0.0.24a N100 firmware bundle. See N100 firmware documentation
  for additional information.
2) Added Charge Complete Message
3) Now supports Ped Tracker API version 1.5.0.0 with SysLog configuration and Archive Erase.

### V0.8.1.5a  210414 ###
1) Built using V1.0.0.10a N100 firmware bundle. See N100 firmware documentation
  for additional information.
2) Stabilizes archiving, fixes archive corruption issue preventing long-term offline caching
3) Fixes bug in SysLog no longer logging after erase, requiring full reset to get it working again.
4) Bug fixes archive erase causing corruption.
5) Stabilization improvements in Kernel and CP-Flex platform.
6) 6) Fixes battery charge complete issue.
7) Fixes 868 restricted issue.

### V0.8.0.0  210331 ###
1) Built using V1.0.0a N100 firmware bundle. Some improvements are noted below.  See N100 firmware documentation
  for additional information.
2) Updated App to compile with v1.1.1 of CP-Flex Platform 
3) Added Out of network (OON) Caching
4) Added Link Check support
5) Fixed bug sending only confirmed messages, new implementation now supports uncofirmed By
   default confirmation for location measurements are managed by the LinkCheck module.
6) Further stablized Archiving implementation by fixing memory leaks and adding additional error checking.
7) New implementation of the radio uplink processing should improve consistency and reliability.

### V0.7.0.6 210326 ###
1) Updated App to compile with v1.1.0.4 of CP-Flex Platform 
2) Updated to reference firmware bundle v0.15.0.4

### V0.6.5.5a 210225 ###
1) Updated app to compile with v1.0.2.0 version of the CP-Flex Platform
2) Set firmware bundle dependency to v0.14.0.2_a with improved join management logic
3) This firmware attempts to address reports that devices will not rejoin the network under
   certain conditions, particularly in the EU region, where a full reset is required to get it 
   to work again.
4) Refactored App to use new V1 platform standard modules.
5) Added persistence to Location Configuration changes.

**2/10/21 - V0.6.5.1a**

6) Updated Battery reporting to 5%/2% differentials.
7) LED UI changed to 3x Blink Orange when tracking deactivated (was 3x Red)
8) Locate messages are now always of unconfirmed type
9) Various Bug fixes

**2/09/21 - V0.6.5.0a**

10) Added NVM parameter support.
11) battery.p no longer sends duplicate battery_ReportStatus on battery monitor updates (10% notifications)
12) Added 2 minute lockout behavior on boot.  (note:  This is a temporary workaround, pending STM enhancements)

### V0.6.4.0 210126 ###
1) Enabled tracking be default.

### V0.6.3.0 210125 ###

1) Added support for power saving state management of the tracking state.  Now disables tracking if entering power save and automatically re-enables tracking when exiting power-save if previously enabled.
2) Implemented release notes file.
---
*Copyright 2019-2021, Codepoint Technologies,* 
*All Rights Reserved*
