@ECHO OFF
if [%1]==[] goto usage

set specfile="cphub_tag_spec.json"
set tag=%1
ECHO Pushing Traxmate Demo Application version %1

cphub push -v -l -s %specfile% cpflexapp "./traxmate_app.bin" traxmate/apps/demo_app:%tag%

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1

