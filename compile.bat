@ECHO OFF
if [%1]==[] goto usage

set lib=../../v1/lib

set _DATE=%date%
set _TIME=%time%
set _VERSION=%1

REM Display Header.
ECHO Compiling Traxmate Demo Application: 
ECHO Version: %_VERSION%
ECHO Date: %_DATE%
ECHO Time: %_TIME%
 

REM Create Version File with string data.
ECHO stock const __VERSION__{} = "%_VERSION%"; > src/version.i
ECHO stock const __DATE__{} = "%_DATE%"; >>		 src/version.i
ECHO stock const __TIME__{} = "%_TIME%"; >>		 src/version.i

pawncc ^
	%lib%/Battery.p %lib%/SimpleTracking.p %lib%/LocationConfig.p ^
	%lib%/NvmRecTools.p %lib%/TelemMgr.p  emergency.p app.p ^
	-Dsrc -S256 -X32768 -XD4092 -o../traxmate_app.bin ^
	-i../../v1/include -i%lib% -i.

goto :eof
:usage
@echo Usage: %0 ^<version^> 
exit /B 1

