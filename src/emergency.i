/**
 *  Name:  emergency.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

forward  e911_Init();
forward bool: e911_NotifyEmergency( );