/**
 *  Name:  emergency.p
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */
 #include "emergency.i"
 #include "telemetry.i"
 

/*************************
* API
**************************/

stock e911_Init()
{
	//Nothing todo.
}

stock bool: e911_NotifyEmergency( )
{
	new ResultCode: rc;
	new Sequence:seqOut;

	rc = TelemSendSeq_Begin( seqOut, 100); 
	if( rc != RC_success)
		return false;

	rc |= 	TelemSendInt32Msg( seqOut, NID_PedCmdsEmergency, 
		NID_PedCmdsEmergencyEnable, MP_critical, MC_alert);

	TelemSendSeq_End( seqOut, rc != RC_success, true);
	return true;
}
